## Сборка проекта
```shell
npm run build
```

### Доступные props для Iframe
```ts
  url: string; // Обязательный параметр, url страницы
```
```ts
  title?: IframeHTMLAttributes['title']; // Опциональный параметр, заголовок iframe
```
```ts
  height?: IframeHTMLAttributes['height']; // Опциональный параметр, высота iframe
```
```ts
  width?: IframeHTMLAttributes['width']; // Опциональный параметр, ширина страницы
```
```ts
  statusOn: boolean; // Опциональный параметр, когда true проверяет статус код, иначе отображает любую страницу
```
```ts
  statusReturn: number[]; // Опциональный параметр, работает в связке с statusOn, передается массив статусов, если хоть один соответствует - отображает страницу
```

### Доступные emits для Iframe
```js
  emit('error') // Сообщает об ошибке
```
```js
  emit('load') // Cообщает об успешном отображении страницы
```

## Глобальное пространство имён
Vue

## Подключение 

```ts
// main.ts
import { createApp } from 'vue'
import {component} from 'yoso' // Подключение
import App from './App.vue'

const app = createApp(App)
app.use(component)
app.mount('#app')
```

```vue
/**
 * App.vue
 */
<template>
  <Iframe :url="'https://www.bing.com/'"/>
</template>
```


