import { IframeHTMLAttributes } from "vue";

export type PropsIframe = {
  url: string;
  title?: IframeHTMLAttributes['title'];
  height?: IframeHTMLAttributes['height'];
  width?: IframeHTMLAttributes['width'];
  statusOn?: boolean;
  statusReturn?: number[];
}

export type EmitIframe = {
  (e: 'error', value: string): unknown;
  (e: 'load', value: string): unknown;
}

export const checkUrl = (url: string): boolean => {
  try {
    new URL(url);
    return true;
  } catch {
    return false;
  }
}