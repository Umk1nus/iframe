import Iframe from "./Iframe.vue";
import { type PropsIframe } from "./iframe";

const component =  {
  install: (app: { component: (a: string, b: unknown) => void }) => {
    app.component("Iframe", Iframe)
  },
}

export {PropsIframe, component}