import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import dts from "vite-plugin-dts";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/index.ts'),
      name: 'yosoComponent',
      formats: ["iife", "es", 'umd'],
      fileName: (format) => `yoso.${format}.js`
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: 'Vue',
        }
      }
    }
  },
  plugins: [vue({ reactivityTransform: true }), dts({
    outDir: 'dist/types',
    include: 'src/**'
  })],
})
