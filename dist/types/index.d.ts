import { type PropsIframe } from "./iframe";
declare const component: {
    install: (app: {
        component: (a: string, b: unknown) => void;
    }) => void;
};
export { PropsIframe, component };
