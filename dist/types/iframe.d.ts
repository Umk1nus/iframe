import { IframeHTMLAttributes } from "vue";
export type PropsIframe = {
    url: string;
    title?: IframeHTMLAttributes['title'];
    height?: IframeHTMLAttributes['height'];
    width?: IframeHTMLAttributes['width'];
    statusOn?: boolean;
    statusReturn?: number[];
};
export type EmitIframe = {
    (e: 'error', value: string): unknown;
    (e: 'load', value: string): unknown;
};
export declare const checkUrl: (url: string) => boolean;
