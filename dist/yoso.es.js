import { defineComponent as y, ref as o, watch as k, openBlock as p, createElementBlock as m, createCommentVNode as E, createElementVNode as L, mergeProps as R } from "vue";
const g = (n) => {
  try {
    return new URL(n), !0;
  } catch {
    return !1;
  }
}, w = { key: 0 }, x = ["src"], B = /* @__PURE__ */ y({
  __name: "Iframe",
  props: {
    url: {},
    title: {},
    height: {},
    width: {},
    statusOn: { type: Boolean },
    statusReturn: {}
  },
  emits: ["error", "load"],
  setup(n, { emit: f }) {
    const r = n, l = f, { statusOn: v, statusReturn: u, ...h } = r, s = o(!1), a = o(!1), t = o(""), c = (e) => {
      a.value = !0, l("error", e);
    }, d = () => {
      s.value = !0, l("load", "Страница загружена и готова к показу");
    }, _ = () => {
      if (v) {
        const e = new XMLHttpRequest();
        e.onreadystatechange = () => {
          (u ? u.includes(e.status) : e.status === 200) ? d() : (s.value = !1, c("Неверный url"));
        }, e.open("GET", t.value), e.send();
      } else
        d();
    };
    return k(() => r.url, (e) => {
      a.value = !1, s.value = !1, !e && (t.value = e), g(e) ? t.value = e : (s.value = !1, c("Возникла ошибка"));
    }), (e, i) => (p(), m("div", null, [
      t.value && !s.value && !a.value ? (p(), m("p", w, "Loading...")) : E("", !0),
      L("iframe", R({
        style: t.value && s.value ? "display: block" : "display: none",
        src: t.value,
        frameborder: "0"
      }, h, {
        onLoad: i[0] || (i[0] = (U) => t.value && _())
      }), `\r
    `, 16, x)
    ]));
  }
}), C = {
  install: (n) => {
    n.component("Iframe", B);
  }
};
export {
  C as component
};
